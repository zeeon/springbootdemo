# Сервис ... проекта Медицина

## Контрибьютеры
- Евгений Кольяков <e.kolyakov@vks.ru>

## Тестовая документация
- [Allure Server](https://allure.tinkoff.ru/project/746)

## Требования
- Java 11+
- Maven 3.8
- Плагин для IDE: [SonarLint](https://plugins.jetbrains.com/plugin/7973-sonarlint)

## Список используемых технологий
- Spring Boot
- JUnit 5

## Запуск Проекта
`mvn spring-boot:run` 