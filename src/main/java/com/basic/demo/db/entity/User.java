package com.basic.demo.db.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;

@Entity
@Table(name = "user_entity", uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "email"})})
@Data
@NoArgsConstructor
public class User implements Persistable<Long> {

    @Id
    @Column(name = "id",
            nullable = false,
            unique   = true)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "email")
    private String email;

    @Override
    public boolean isNew() {
        return true;
    }
}
