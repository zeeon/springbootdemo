package com.basic.demo.db.service;



import com.basic.demo.db.entity.User;
import com.basic.demo.db.repository.UserRepository;
import com.basic.demo.exceptions.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository repository;
    private final JdbcTemplate jdbcTemplate;


    public List<User> getList(){
        return repository.findAll();
    }

    public User getOne(Long id){
        return repository.findById(id).orElseThrow(() -> new UserNotFoundException("User not found by id: " + id));
    }

    public User create(User user){
        return repository.save(user);
    }

    public User update(Long id, User newUser){
        User user1 = repository.findById(id)
                .map(user -> {
                    user.setName(newUser.getName());
                    user.setSurname(newUser.getSurname());
                    user.setEmail(newUser.getEmail());
                    return repository.save(user);
                })
                .orElseThrow(()-> new UserNotFoundException("User not found by id: " + id));
        return user1;
    }

    public void delete(Long id){
        User byId = repository.findById(id).orElseThrow(()-> new UserNotFoundException("User not found by id: " + id));
        repository.deleteById(byId.getId());
    }
}
