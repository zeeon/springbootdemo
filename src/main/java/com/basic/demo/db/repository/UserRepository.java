package com.basic.demo.db.repository;

import com.basic.demo.db.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Override
    @Query(nativeQuery = true, value = "delete from user_entity where id = :id")
    @Modifying
    void deleteById(Long id);
}