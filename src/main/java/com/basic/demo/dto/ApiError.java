package com.basic.demo.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ApiError {

    private String message;
    private String path;
    private Integer status;
    private LocalDateTime timestamp = LocalDateTime.now();

    public ApiError(String message, String path, Integer status) {
        this.message = message;
        this.path = path;
        this.status = status;
    }
}
