package com.basic.demo.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;


@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {
    @JsonProperty(required = true)
    @NotNull
    private Long id;

    private String name;

    private String surname;

    @JsonProperty(required = true)
    @NotEmpty @NotBlank @Email
    private String email;
}
