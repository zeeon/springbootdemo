package com.basic.demo.controllers;

import com.basic.demo.db.entity.User;
import com.basic.demo.db.service.UserService;
import com.basic.demo.dto.UserDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping(path = {"/api/v1/users"}, produces = APPLICATION_JSON_VALUE)
public class UserController {
    private final ModelMapper modelMapper;
    private final UserService userService;

    private static final String NEW_USER_LOG = "New user was created id:{}";
    private static final String USER_UPDATED_LOG = "User:{} was updated";
    private static final String USER_DELETED_LOG = "User with id:{} was deleted";


    @Operation(summary = "Получение списка пользователей")
    @ApiResponse(responseCode = "200", description = "List<User> получен", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(type = "array", implementation = UserDto.class))})
    @GetMapping
    public ResponseEntity<List<UserDto>> getUsers() {
        List<User> users = userService.getList();
        List<UserDto> dtos = users
                .stream()
                .map(user -> modelMapper.map(user, UserDto.class))
                .collect(Collectors.toList());
        return ResponseEntity.ok(dtos);
    }

    @Operation(summary = "Получение user по {id}")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Найден user", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class))}),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)})
    @GetMapping(path = "/{id}")
    public ResponseEntity<UserDto> loadUser(@PathVariable(value = "id") Long id) {
        User user = userService.getOne(id);
        return ResponseEntity.ok(modelMapper.map(user, UserDto.class));
    }

    @Operation(summary = "Update an user by its id")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "User was updated", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class))}),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)})
    @PutMapping(path = "/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> updateCustomQuoteRequest(
            @PathVariable(value = "id") Long id,
            @Valid @RequestBody UserDto userDto) {
        final User updateUser =
                userService.update(id, modelMapper.map(userDto, User.class));
        log.info(USER_UPDATED_LOG, updateUser);
        return ResponseEntity.ok(modelMapper.map(updateUser, UserDto.class));
    }


    @Operation(summary = "Создание пользователя")
    @ApiResponse(responseCode = "201", description = "User создан", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class))})
    @PostMapping
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        userService.create(user);
        log.info(NEW_USER_LOG, user);
        return ResponseEntity.status(HttpStatus.CREATED).body(userDto);
    }

    @Operation(summary = "Удаление пользователя")
    @ApiResponse(responseCode = "204", description = "User удален")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteUser(@Valid @PathVariable(value = "id") Long id) {
        userService.delete(id);
        log.info(USER_DELETED_LOG, id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
