package com.basic.demo;

import com.basic.demo.controllers.UserController;
import com.basic.demo.db.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class DemoApplicationTests {
    @Autowired
    private UserController userController;

    @Test
    void contextLoads() {
        assertThat(userController)
                .as("Check usercontroller loads to context")
                .isNotNull();
    }
}
